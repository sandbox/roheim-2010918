-- REQUIREMENTS --

* Drupal 7

-- INSTALLATION --

* Install as normal: http://drupal.org/node/70151.

-- CONTACT INFO --

Current maintainer(s):

* David Roheim (roheim) - https://drupal.org/user/384960

<?php
/**
 * @file
 * Admin for the Clean Cache module
 */


/**
 * Administration settings form.
 */
function clean_cache_admin_form($form, &$form_state) {
  $form['clean_cache_enable_cache_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Form cache'),
    '#description' => t('Enables the cleaning of expired records from the form cache'),
    '#default_value' => variable_get('clean_cache_enable_cache_form', TRUE),
  );

  return system_settings_form($form);
}
